<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    /**
     * @Route("/")
     */
    public function number()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>Lucky number: ' . $number . '</body></html>'
        );
    }
    /**
     * @Route("/extra")
     */
    public function another()
    {
        $number = random_int(0, 100);

        return new Response(
            '<html><body>extra page: ' . $number . '</body></html>'
        );
    }
}