<?php
namespace App\GraphQL\Resolver;

use App\Entity\Ding;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

Class DingResolver implements ResolverInterface
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }

    public function resolve(string $id) :Ding
    {
        return $this->em->find(Ding::class, $id);
    }

    public function name(Ding $author) :string
    {
        return $author->getName();
    }

}