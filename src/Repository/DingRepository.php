<?php

namespace App\Repository;

use App\Entity\Ding;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ding|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ding|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ding[]    findAll()
 * @method Ding[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ding::class);
    }

    // /**
    //  * @return Ding[] Returns an array of Ding objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ding
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
